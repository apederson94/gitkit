package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property description
 * @property endingAt
 * @property id
 * @property panelId
 * @property startingAt
 */
data class MetricsDashboard(
        val description: String? = null,
        val endingAt: Time? = null,
        val id: ID,
        val panelId: String? = null,
        val startingAt: Time? = null
)