package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property revision Revision
 * @property version Version
 */
data class Metadata(
        val revision: String,
        val version: String
)