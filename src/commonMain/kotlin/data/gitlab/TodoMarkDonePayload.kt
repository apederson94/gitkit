package data.gitlab

import data.gitlab.CommonParameterInterfaces.GitLabPayload

/**
 * Autogenerated return type of TodoMarkDone
 *
 * @property todo The requested todo
 * @see GitLabPayload for other params
 *
 */
data class TodoMarkDonePayload(
        override val clientMutationId: String? = null,
        override val errors: MutableList<String>? = null,
        val todo: Todo
) : GitLabPayload