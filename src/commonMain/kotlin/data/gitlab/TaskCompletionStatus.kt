package data.gitlab

/**
 * Completion status of tasks
 *
 * @property completedCount Number of completed tasks
 * @property count Number of total tasks
 */
data class TaskCompletionStatus(
        val completedCount: Int,
        val count: Int
)