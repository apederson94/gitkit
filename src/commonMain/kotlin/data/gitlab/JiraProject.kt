package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property key Key of the Jira project
 * @property name Name of the Jira project
 * @property projectId ID of the Jira project
 */
 data class JiraProject(
        val key: String,
        val name: String? = null,
        val projectId: Int
)