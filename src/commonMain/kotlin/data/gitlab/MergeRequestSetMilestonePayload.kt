package data.gitlab

import data.gitlab.CommonParameterInterfaces.GitLabPayload

/**
 * Autogenerated return type of MergeRequestSetMilestone
 *
 * @property mergeRequest The merge request after mutation
 * @see GitLabPayload for other params
 *
 */
data class MergeRequestSetMilestonePayload(
        override val clientMutationId: String? = null,
        override val errors: MutableList<String>? = null,
        val mergeRequest: MergeRequest? = null
) : GitLabPayload