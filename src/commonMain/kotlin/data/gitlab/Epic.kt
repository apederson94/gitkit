package data.gitlab

/**
 * Represents an epic.
 *
 * @property author Author of the epic
 * @property closedAt Timestamp of the epic’s closure
 * @property confidential Indicates if the epic is confidential
 * @property createdAt Timestamp of the epic’s creation
 * @property descendantCounts Number of open and closed descendant epics and issues
 * @property descendantWeightSum Total weight of open and closed issues in the epic and its descendants
 * @property description Description of the epic
 * @property downvotes Number of downvotes the epic has received
 * @property dueDate Due date of the epic
 * @property dueDateFixed Fixed due date of the epic
 * @property dueDateFromMilestones Inherited due date of the epic from milestones
 * @property dueDateIsFixed Indicates if the due date has been manually set
 * @property group Group to which the epic belongs
 * @property hasChildren Indicates if the epic has children
 * @property hasIssues Indicates if the epic has direct issues
 * @property hasParent Indicates if the epic has a parent epic
 * @property healthStatus Current health status of the epic
 * @property id ID of the epic
 * @property iid Internal ID of the epic
 * @property parent Parent epic of the epic
 * @property reference Internal reference of the epic. Returned in shortened format by default
 * @property relationPath URI path of the epic-issue relationship
 * @property relativePosition The relative position of the epic in the epic tree
 * @property startDate Start date of the epic
 * @property startDateFixed Fixed start date of the epic
 * @property startDateFromMilestones Inherited start date of the epic from milestones
 * @property startDateIsFixed Indicates if the start date has been manually set
 * @property state State of the epic
 * @property subscribed Indicates the currently logged in user is subscribed to the epic
 * @property title Title of the epic
 * @property updatedAt Timestamp of the epic’s last activity
 * @property upvotes Number of upvotes the epic has received
 * @property userPermissions Permissions for the current user on the resource
 * @property webPath Web path of the epic
 * @property webUrl Web URL of the epic
 */
data class Epic(
        val author: User,
        val closedAt: Time? = null,
        val confidential: Boolean? = null,
        val createdAt: Time? = null,
        val descendantCounts: EpicDescendantCount? = null,
        val descendantWeightSum: EpicDescendantWeights? = null,
        val description: String? = null,
        val downvotes: Int,
        val dueDate: Time? = null,
        val dueDateFixed: Time? = null,
        val dueDateFromMilestones: Time? = null,
        val dueDateIsFixed: Time? = null,
        val group: Group,
        val hasChildren: Boolean,
        val hasIssues: Boolean,
        val hasParent: Boolean,
        val healthStatus: EpicHealthStatus? = null,
        val id: ID,
        val iid: ID,
        val parent: Epic? = null,
        val reference: String,
        val relationPath: String? = null,
        val relativePosition: Int? = null,
        val startDate: Time? = null,
        val startDateFixed: Time? = null,
        val startDateFromMilestones: Time? = null,
        val startDateIsFixed: Boolean? = null,
        val state: EpicState,
        val subscribed: Boolean,
        val title: String? = null,
        val updatedAt: Time? = null,
        val upvotes: Int,
        val userPermissions: EpicPermissions,
        val webPath: String,
        val webUrl: String
) {
     fun test() {
         Epic()
     }
}