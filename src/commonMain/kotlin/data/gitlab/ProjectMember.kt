package data.gitlab

/**
 * Member of a project
 *
 * @property accessLevel Access level of the member
 * @property id ID of the member
 * @property user User that is associated with the member object
 */
data class ProjectMember(
        val accessLevel: Int,
        val id: ID,
        val user: User
)