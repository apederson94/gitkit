package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property avatarUrl URL of the user’s avatar
 * @property id ID of the user
 * @property name Human-readable name of the user
 * @property state State of the user
 * @property userPermissions Permissions for the current user on the resource
 * @property username Username of the user. Unique within this instance of GitLab
 * @property webUrl Web URL of the user
 */
data class User(
        val avatarUrl: String?,
        val id: ID,
        val name: String,
        val state: UserState,
        val userPermissions: UserPermissions,
        val username: String,
        val webUrl: String
)