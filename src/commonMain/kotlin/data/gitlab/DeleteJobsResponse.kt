package data.gitlab

/**
 * The response from the AdminSidekiqQueuesDeleteJobs mutation.
 *
 * @property completed Whether or not the entire queue was processed in time; if not, retrying the same request is safe
 * @property deletedJobs The number of matching jobs deleted
 * @property queueSize The queue size after processing
 */
data class DeleteJobsResponse(
    val completed: Boolean? = null,
    val deletedJobs: Int? = null,
    val queueSize: Int? = null
)