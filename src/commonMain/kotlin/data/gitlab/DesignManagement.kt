package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property designAtVersion Find a design as of a version
 * @property version Find a version
 */
data class DesignManagement(
        val designAtVersion: DesignAtVersion? = null,
        val version: DesignVersion? = null
)