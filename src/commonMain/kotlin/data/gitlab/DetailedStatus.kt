package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property detailsPath Path of the details for the pipeline status
 * @property favicon Favicon of the pipeline status
 * @property group Group of the pipeline status
 * @property hasDetails Indicates if the pipeline status has further details
 * @property icon Icon of the pipeline status
 * @property label Label of the pipeline status
 * @property text Text of the pipeline status
 * @property tooltip Tooltip associated with the pipeline status
 */
data class DetailedStatus(
        val detailsPath: String,
        val favicon: String,
        val group: String,
        val hasDetails: Boolean,
        val icon: String,
        val label: String,
        val text: String,
        val tooltip: String
)