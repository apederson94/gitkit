package data.gitlab

/**
 * Represents the snippet blob
 *
 * @property binary Shows whether the blob is binary
 * @property externalStorage Blob external storage
 * @property mode Blob mode
 * @property name Blob name
 * @property path Blob path
 * @property plainData Blob plain highlighted data
 * @property rawPath Blob raw content endpoint path
 * @property renderedAsText Shows whether the blob is rendered as text
 * @property richData Blob highlighted data
 * @property richViewer Blob content rich viewer
 * @property simpleViewer Blob content simple viewer
 * @property size Blob size
 */
data class SnippetBlob(
        val binary: Boolean,
        val externalStorage: String?,
        val mode: String?,
        val name: String?,
        val path: String?,
        val plainData: String?,
        val rawPath: String,
        val renderedAsText: Boolean,
        val richData: String?,
        val richViewer: SnippetBlobViewer?,
        val simpleViewer: SnippetBlobViewer,
        val size: Int
)