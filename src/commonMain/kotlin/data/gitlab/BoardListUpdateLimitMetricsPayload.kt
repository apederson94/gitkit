package data.gitlab

import data.gitlab.CommonParameterInterfaces.GitLabPayload

/**
 * Autogenerated return type of BoardListUpdateLimitMetrics
 *
 * @property list The updated list
 * @see GitLabPayload for other params
 */
data class BoardListUpdateLimitMetricsPayload(
        override val clientMutationId: String? = null,
        override val errors: MutableList<String>? = null,
        val list: BoardList? = null
) : GitLabPayload