package data.gitlab

/**
 * Represents a vulnerable package. Used in vulnerability dependency data
 *
 * @property name The name of the vulnerable package
 */
data class VulnerablePackage(
        val name: String?
)