package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property format Format of the source
 * @property url Download URL of the source
 */
data class ReleaseSource(
        val format: String?,
        val url: String?
)