package data.gitlab

/**
 * Description not provided by API documentation
 * 
 * @property author Author of the commit
 * @property authorGravatar Commit authors gravatar
 * @property authorName Commit authors name
 * @property authoredDate Timestamp of when the commit was authored
 * @property description Description of the commit message
 * @property id ID (global ID) of the commit
 * @property message Raw commit message
 * @property sha SHA1 ID of the commit
 * @property signatureHtml Rendered HTML of the commit signature
 * @property title Title of the commit message
 * @property titleHtml The GitLab Flavored Markdown rendering of [title]
 * @property webUrl Web URL of the commit
 * 
 */
data class Commit(
        val author: User? = null,
        val authorGravatar: String? = null,
        val authorName: String? = null,
        val authoredDate: String? = null,
        val description: String? = null,
        val id: ID,
        val message: String? = null,
        val sha: String,
        val signatureHtml: String? = null,
        val title: String? = null,
        val titleHtml: String? = null,
        val webUrl: String
)