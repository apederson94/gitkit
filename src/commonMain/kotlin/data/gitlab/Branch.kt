package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property commit Commit for the branch
 * @property name Name of the branch
 */
data class Branch(
        val commit: Commit? = null,
        val name: String
)