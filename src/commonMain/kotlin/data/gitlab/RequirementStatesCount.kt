package data.gitlab

/**
 * Counts of requirements by their state.
 *
 * @property archived Number of archived requirements
 * @property opened Number of opened requirements
 */
data class RequirementStatesCount(
        val archived: Int?,
        val opened: Int?
)