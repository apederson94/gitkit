package data.gitlab

/**
 * State of a Sentry error
 *
 * @property level Severity level of the Sentry Error
 * @property logger Logger of the Sentry Error
 */
data class SentryErrorTags(
        val level: String?,
        val logger: String?
)