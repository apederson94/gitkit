package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property issue The issue that logged time was added to
 * @property spentAt Timestamp of when the time tracked was spent at
 * @property timeSpent The time spent displayed in seconds
 * @property user The user that logged the time
 */
data class Timelog(
        val issue: Issue?,
        val spentAt: Time?,
        val timeSpent: Int,
        val user: User
)