package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property beforeSha Base SHA of the source branch
 * @property committedAt Timestamp of the pipeline’s commit
 * @property coverage Coverage percentage
 * @property createdAt Timestamp of the pipeline’s creation
 * @property detailedStatus Detailed status of the pipeline
 * @property duration Duration of the pipeline in seconds
 * @property finishedAt Timestamp of the pipeline’s completion
 * @property id ID of the pipeline
 * @property iid Internal ID of the pipeline
 * @property sha SHA of the pipeline’s commit
 * @property startedAt Timestamp when the pipeline was started
 * @property status Status of the pipeline (CREATED, WAITING_FOR_RESOURCE, PREPARING, PENDING, RUNNING, FAILED, SUCCESS, CANCELED, SKIPPED, MANUAL, SCHEDULED)
 * @property updatedAt Timestamp of the pipeline’s last activity
 * @property userPermissions Permissions for the current user on the resource
 */
data class Pipeline(
        val beforeSha: String?,
        val committedAt: Time?,
        val coverage: Float?,
        val createdAt: Time,
        val detailedStatus: DetailedStatus,
        val duration: Int?,
        val finishedAt: Time?,
        val id: ID,
        val iid: String,
        val sha: String,
        val startedAt: Time?,
        val status: PipelineStatusEnum,
        val updatedAt: Time,
        val userPermissions: PipelinePermissions
)