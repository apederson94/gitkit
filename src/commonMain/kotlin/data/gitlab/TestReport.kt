package data.gitlab

/**
 * Represents a requirement test report
 *
 * @property author Author of the test report
 * @property createdAt Timestamp of when the test report was created
 * @property id ID of the test report
 * @property pipeline Pipeline that created the test report
 * @property state State of the test report
 */
data class TestReport(
        val author: User?,
        val createdAt: Time,
        val id: ID,
        val pipeline: Pipeline?,
        val state: TestReportState
)