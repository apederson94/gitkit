package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property buildArtifactsSize Build artifacts size of the project
 * @property commitCount Commit count of the project
 * @property lfsObjectsSize Large File Storage (LFS) object size of the project
 * @property packagesSize Packages size of the project
 * @property repositorySize Repository size of the project
 * @property storageSize Storage size of the project
 * @property wikiSize Wiki size of the project
 */
data class ProjectStatistics(
        val buildArtifactsSize: Float,
        val commitCount: Float,
        val lfsObjectsSize: Float,
        val packagesSize: Float,
        val repositorySize: Float,
        val storageSize: Float,
        val wikiSize: Float
)