package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property count Count of errors received since the previously recorded time
 * @property time Time the error frequency stats were recorded
 */
data class SentryErrorFrequency(
        val count: Int,
        val time: Time
)