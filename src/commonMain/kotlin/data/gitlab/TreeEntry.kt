package data.gitlab

/**
 * Represents a directory
 *
 * @property flatPath Flat path of the entry
 * @property id ID of the entry
 * @property name Name of the entry
 * @property path Path of the entry
 * @property sha Last commit sha for the entry
 * @property type Type of tree entry
 * @property webUrl Web URL for the tree entry (directory)
 */
data class TreeEntry(
        val flatPath: String,
        val id: ID,
        val name: String,
        val path: String,
        val sha: String,
        val type: EntryType,
        val webUrl: String?
)