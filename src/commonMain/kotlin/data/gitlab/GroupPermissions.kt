package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property readGroup Indicates the user can perform read_group on this resource
 */
data class GroupPermissions(
        val readGroup: Boolean
)