package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property createdAt Timestamp of the issue’s creation
 * @property enabled Indicates whether Grafana integration is enabled
 * @property grafanaUrl URL for the Grafana host for the Grafana integration
 * @property id Internal ID of the Grafana integration
 * @property updatedAt Timestamp of the issue’s last activity
 */
data class GrafanaIntegration(
        val createdAt: Time,
        val enabled: Boolean,
        val grafanaUrl: String,
        val id: ID,
        val updatedAt: Time
)