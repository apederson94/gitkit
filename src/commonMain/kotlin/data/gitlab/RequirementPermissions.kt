package data.gitlab

/**
 * Check permissions for the current user on a requirement
 *
 * @property adminRequirement Indicates the user can perform admin_requirement on this resource
 * @property createRequirement Indicates the user can perform create_requirement on this resource
 * @property destroyRequirement Indicates the user can perform destroy_requirement on this resource
 * @property readRequirement Indicates the user can perform read_requirement on this resource
 * @property updateRequirement Indicates the user can perform update_requirement on this resource
 */
data class RequirementPermissions(
        val adminRequirement: Boolean,
        val createRequirement: Boolean,
        val destroyRequirement: Boolean,
        val readRequirement: Boolean,
        val updateRequirement: Boolean
)