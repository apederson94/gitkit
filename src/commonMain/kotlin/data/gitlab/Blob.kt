package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property flatPath Flat path of the entry
 * @property id ID of the entry
 * @property lfsOid LFS ID of the blob
 * @property name Name of the entry
 * @property path Path of the entry
 * @property sha Last commit sha for the entry
 * @property type Type of tree entry
 * @property webUrl Web URL of the blob
 */
data class Blob(
        val flatPath: String,
        val id: ID,
        val lfsOid: String? = null,
        val name: String,
        val path: String,
        val sha: String,
        val type: EntryType,
        val webUrl: String? = null
)