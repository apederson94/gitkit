package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property external Indicates the link points to an external resource
 * @property id ID of the link
 * @property linkType Type of the link: other, runbook, image, package; defaults to other
 * @property name Name of the link
 * @property url URL of the link
 */
data class ReleaseLink(
        val external: Boolean?,
        val id: ID,
        val linkType: ReleaseLinkType?,
        val name: String?,
        val url: String?
)