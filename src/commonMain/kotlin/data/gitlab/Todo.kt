package data.gitlab

/**
 * Representing a todo entry
 *
 * @property action Action of the todo
 * @property author The owner of this todo
 * @property body Body of the todo
 * @property createdAt Timestamp this todo was created
 * @property group Group this todo is associated with
 * @property id ID of the todo
 * @property project The project this todo is associated with
 * @property state State of the todo
 * @property targetType Target type of the todo
 */
data class Todo(
        val action: TodoActionEnum,
        val author: User,
        val body: String,
        val createdAt: Time,
        val group: Group?,
        val id: ID,
        val project: Project?,
        val state: TodoStateEnum,
        val targetType: TodoTargetEnum
)