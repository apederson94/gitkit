package data.gitlab.CommonParameterInterfaces

/**
 * Generic Gitlab Payload parameters are stored here
 * @property clientMutationId A unique identifier for the client performing the mutation.
 * @property errors Errors encountered during execution of the mutation.
 */
interface GitLabPayload {
    val clientMutationId: String?
    val errors: MutableList<String>?
}