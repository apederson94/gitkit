package data.gitlab

/**
 * Represents a vulnerable dependency. Used in vulnerability location data
 *
 * @property vulnerablePackage The package associated with the vulnerable dependency
 * @property version The version of the vulnerable dependency
 */
data class VulnerableDependency(
        //TODO: this is actually "package" but needs to be serialized to this because package is a keyword
        val vulnerablePackage: VulnerablePackage?,
        val version: String?
)