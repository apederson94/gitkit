package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property adminNote Indicates the user can perform admin_note on this resource
 * @property awardEmoji Indicates the user can perform award_emoji on this resource
 * @property createNote Indicates the user can perform create_note on this resource
 * @property readNote Indicates the user can perform read_note on this resource
 * @property resolveNote Indicates the user can perform resolve_note on this resource
 */
data class NotePermissions(
        val adminNote: Boolean,
        val awardEmoji: Boolean,
        val createNote: Boolean,
        val readNote: Boolean,
        val resolveNote: Boolean
)