package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property empty Indicates repository has no visible content
 * @property exists Indicates a corresponding Git repository exists on disk
 * @property rootRef Default branch of the repository
 * @property tree Tree of the repository
 */
data class Repository(
        val empty: Boolean,
        val exists: Boolean,
        val rootRef: String?,
        val tree: Tree?
)