package data.gitlab

/**
 * Describes where code is deployed for a project
 *
 * @property id ID of the environment
 * @property metricsDashboard Metrics dashboard schema for the environment
 * @property name Human-readable name of the environment
 * @property state State of the environment, for example: available/stopped
 */
data class Environment(
        val id: ID,
        val metricsDashboard: MetricsDashboard? = null,
        val name: String,
        val state: String
)