package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property allowCollaboration Indicates if members of the target project can push to the fork
 * @property author User who created this merge request
 * @property createdAt Timestamp of when the merge request was created
 * @property defaultMergeCommitMessage Default merge commit message of the merge request
 * @property description Description of the merge request (Markdown rendered as HTML for caching)
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property diffHeadSha Diff head SHA of the merge request
 * @property diffRefs References of the base SHA, the head SHA, and the start SHA for this merge request
 * @property discussionLocked Indicates if comments on the merge request are locked to members only
 * @property downvotes Number of downvotes for the merge request
 * @property forceRemoveSourceBranch Indicates if the project settings will lead to source branch deletion after merge
 * @property headPipeline The pipeline running on the branch HEAD of the merge request
 * @property id ID of the merge request
 * @property iid Internal ID of the merge request
 * @property inProgressMergeCommitSha Commit SHA of the merge request if merge is in progress
 * @property mergeCommitSha SHA of the merge request commit (set once merged)
 * @property mergeError Error message due to a merge error
 * @property mergeOngoing Indicates if a merge is currently occurring
 * @property mergeStatus Status of the merge request
 * @property mergeWhenPipelineSucceeds Indicates if the merge has been set to be merged when its pipeline succeeds (MWPS)
 * @property mergeDiscussionsSate Indicates if all discussions in the merge request have been resolved, allowing the merge request to be merged
 * @property mergedAt Timestamp of when the merge request was merged, null if not merged
 * @property milestone The milestone of the merge request
 * @property project Alias for target_project
 * @property projectId ID of the merge request project
 * @property rebaseCommitSha Rebase commit SHA of the merge request
 * @property rebaseInProgress Indicates if there is a rebase currently in progress for the merge request
 * @property reference Internal reference of the merge request. Returned in shortened format by default
 * @property shouldBeRebased Indicates if the merge request will be rebased
 * @property shouldRemoveSourceBranch Indicates if the source branch of the merge request will be deleted after merge
 * @property sourceBranch Source branch of the merge request
 * @property sourceBranchExists Indicates if the source branch of the merge request exists
 * @property sourceProject Source project of the merge request
 * @property sourceProjectId ID of the merge request source project
 * @property state State of the merge request
 * @property subscribed Indicates if the currently logged in user is subscribed to this merge request
 * @property targetBranch Target branch of the merge request
 * @property targetBranchExists Indicates if the target branch of the merge request exists
 * @property targetProject Target project of the merge request
 * @property targetProjectId ID of the merge request target project
 * @property taskCompletionStatus Completion status of tasks
 * @property timeEstimate Time estimate of the merge request
 * @property title Title of the merge request
 * @property titleHtml The GitLab Flavored Markdown rendering of [title]
 * @property totalTimeSpent Total time reported as spent on the merge request
 * @property updatedAt Timestamp of when the merge request was last updated
 * @property upvotes Number of upvotes for the merge request
 * @property userNotesCount User notes count of the merge request
 * @property userPermissions Permissions for the current user on the resource
 * @property webUrl Web URL of the merge request
 * @property workInProgress Indicates if the merge request is a work in progress (WIP)
 */
data class MergeRequest(
        val allowCollaboration: Boolean? = null,
        val author: User,
        val createdAt: Time,
        val defaultMergeCommitMessage: String? = null,
        val description: String? = null,
        val descriptionHtml: String? = null,
        val diffHeadSha: String? = null,
        val diffRefs: DiffRefs? = null,
        val discussionLocked: Boolean,
        val downvotes: Int,
        val forceRemoveSourceBranch: Boolean? = null,
        val headPipeline: Pipeline? = null,
        val id: ID,
        val iid: String!,
        val inProgressMergeCommitSha: String? = null,
        val mergeCommitSha: String? = null,
        val mergeError: String? = null,
        val mergeOngoing: Boolean,
        val mergeStatus: String? = null,
        val mergeWhenPipelineSucceeds: Boolean? = null,
        val mergeDiscussionsSate: Boolean? = null,
        val mergedAt: Time? = null,
        val milestone: Milestone? = null,
        val project: Project,
        val projectId: Int,
        val rebaseCommitSha: String? = null,
        val rebaseInProgress: Boolean,
        val reference: String,
        val shouldBeRebased: Boolean,
        val shouldRemoveSourceBranch: Boolean? = null,
        val sourceBranch: String,
        val sourceBranchExists: String,
        val sourceProject: Project? = null,
        val sourceProjectId: Int? = null,
        val state: MergeRequestState,
        val subscribed: Boolean,
        val targetBranch: String,
        val targetBranchExists: Boolean,
        val targetProject: Project,
        val targetProjectId: Int,
        val taskCompletionStatus: TaskCompletionStatus,
        val timeEstimate: Int,
        val title: String,
        val titleHtml: String? = null,
        val totalTimeSpent: Int,
        val updatedAt: Time,
        val upvotes: Int,
        val userNotesCount: Int? = null,
        val userPermissions: MergeRequestPermissions,
        val webUrl: String? = null,
        val workInProgress: Boolean
)