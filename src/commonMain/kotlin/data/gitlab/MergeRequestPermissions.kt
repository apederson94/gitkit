package data.gitlab

/**
 * Check permissions for the current user on a merge request
 *
 * @property adminMergeRequest Indicates the user can perform admin_merge_request on this resource
 * @property cherryPickOnCurrentMergeRequest Indicates the user can perform cherry_pick_on_current_merge_request on this resource
 * @property createNote Indicates the user can perform create_note on this resource
 * @property pushToSourceBranch Indicates the user can perform push_to_source_branch on this resource
 * @property readMergeRequest Indicates the user can perform read_merge_request on this resource
 * @property removeSourceBranch Indicates the user can perform remove_source_branch on this resource
 * @property revertOnCurrentMergeRequest Indicates the user can perform revert_on_current_merge_request on this resource
 * @property updateMergeRequest Indicates the user can perform update_merge_request on this resource
 */
data class MergeRequestPermissions(
        val adminMergeRequest: Boolean,
        val cherryPickOnCurrentMergeRequest: Boolean,
        val createNote: Boolean,
        val pushToSourceBranch: Boolean,
        val readMergeRequest: Boolean,
        val removeSourceBranch: Boolean,
        val revertOnCurrentMergeRequest: Boolean,
        val updateMergeRequest: Boolean
)