package data.gitlab

import data.gitlab.CommonParameterInterfaces.GitLabPayload

/**
 * Autogenerated return type of CreateRequirement
 *
 * @property requirement The requirement after mutation
 * @see GitLabPayload for other params
 */
data class CreateRequirementPayload(
    override val clientMutationId: String? = null,
    override val errors: MutableList<String>? = null,
    val requirement: Requirement? = null
): GitLabPayload