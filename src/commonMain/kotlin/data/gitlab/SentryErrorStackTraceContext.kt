package data.gitlab

/**
 * An object context for a Sentry error stack trace
 *
 * @property code Code number of the context
 * @property line Line number of the context
 */
data class SentryErrorStackTraceContext(
        val code: String,
        val line: Int
)