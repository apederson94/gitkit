package data.gitlab

/**
 * Relationship between an epic and an issue
 *
 * @property author User that created the issue
 * @property closedAt Timestamp of when the issue was closed
 * @property confidential Indicates the issue is confidential
 * @property createdAt Timestamp of when the issue was created
 * @property description Description of the issue
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property designCollection Collection of design images associated with this issue
 * @property discussionLocked Indicates discussion is locked on the issue
 * @property downvotes Number of downvotes the issue has received
 * @property dueDate Due date of the issue
 * @property epic Epic to which this issue belongs
 * @property epicIssueId ID of the epic-issue relation
 * @property healthStatus Current health status. Returns null if save_issuable_health_status feature flag is disabled.
 * @property id Global ID of the epic-issue relation
 * @property iid Internal ID of the issue
 * @property iteration Iteration of the issue
 * @property milestone Milestone of the issue
 * @property reference Internal reference of the issue. Returned in shortened format by default
 * @property relationPath URI path of the epic-issue relation
 * @property relativePosition Relative position of the issue (used for positioning in epic tree and issue boards)
 * @property state State of the issue
 * @property subscribed Indicates the currently logged in user is subscribed to the issue
 * @property taskCompletionStatus Task completion status of the issue
 * @property timeEstimate Time estimate of the issue
 * @property title Title of the issue
 * @property titleHtml The GitLab Flavored Markdown rendering of [title]
 * @property totalTimeSpent Total time reported as spent on the issue
 * @property updatedAt Timestamp of when the issue was last updated
 * @property upvotes Number of upvotes the issue has received
 * @property userNotesCount Number of user notes of the issue
 * @property userPermissions Permissions for the current user on the resource
 * @property webPath Web path of the issue
 * @property webUrl Web URL of the issue
 * @property weight Weight of the issue
 */
data class EpicIssue(
        val author: User,
        val closedAt: Time? = null,
        val confidential: Boolean,
        val createdAt: Time,
        val description: String? = null,
        val descriptionHtml: String? = null,
        val designCollection: DesignCollection? = null,
        val discussionLocked: Boolean,
        val downvotes: Int,
        val dueDate: Time? = null,
        val epic: Epic? = null,
        val epicIssueId: ID,
        val healthStatus: HealthStatus? = null,
        val id: ID? = null,
        val iid: ID,
        val iteration: Iteration? = null,
        val milestone: Milestone? = null,
        val reference: String,
        val relationPath: String? = null,
        val relativePosition: Int? = null,
        val state: IssueState,
        val subscribed: Boolean,
        val taskCompletionStatus: TaskCompletionStatus,
        val timeEstimate: Int,
        val title: String,
        val titleHtml: String? = null,
        val totalTimeSpent: Int,
        val updatedAt: Time,
        val upvotes: Int,
        val userNotesCount: Int,
        val userPermissions: IssuePermissions,
        val webPath: String,
        val webUrl: String,
        val weight: Int? = null
)