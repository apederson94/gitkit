package data.gitlab

/**
 * An object containing a stack trace entry for a Sentry error
 *
 * @property dateReceived Time the stack trace was received by Sentry
 * @property issueId ID of the Sentry error
 * @property stackTraceEntries Stack trace entries for the Sentry error
 */
data class SentryErrorStackTrace(
        val dateReceived: String,
        val issueId: String,
        val stackTraceEntries: MutableList<SentryErrorStackTraceEntry>?
)