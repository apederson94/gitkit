package data.gitlab

/**
 * Represents an iteration object
 *
 * @property createdAt Timestamp of iteration creation
 * @property description Description of the iteration
 * @property dueDate Timestamp of the iteration due date
 * @property id ID of the iteration
 * @property startDate Timestamp of the iteration start date
 * @property state State of the iteration
 * @property title Title of the iteration
 * @property updatedAt Timestamp of last iteration update
 * @property webPath Web path of the iteration
 * @property webUrl Web URL of the iteration
 */
data class Iteration(
        val createdAt: Time,
        val description: String? = null,
        val dueDate: Time? = null,
        val id: ID,
        val startDate: Time? = null,
        val state: IterationState,
        val title: String,
        val updatedAt: Time,
        val webPath: String,
        val webUrl: String
)