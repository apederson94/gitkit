package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property containerRepositoriesMaxCapacity The maximum concurrency of container repository sync for this secondary node
 * @property enabled Indicates whether this Geo node is enabled
 * @property filesMaxCapacity The maximum concurrency of LFS/attachment backfill for this secondary node
 * @property id ID of this GeoNode
 * @property internalUrl The URL defined on the primary node that secondary nodes should use to contact it
 * @property minimumReverificationInterval The interval (in days) in which the repository verification is valid. Once expired, it will be reverified
 * @property name The unique identifier for this Geo node
 * @property primary Indicates whether this Geo node is the primary
 * @property reposMaxCapacity The maximum concurrency of repository backfill for this secondary node
 * @property selectiveSyncShards The repository storages whose projects should be synced, if selective_sync_type == shards
 * @property selectiveSyncType Indicates if syncing is limited to only specific groups, or shards
 * @property syncObjectsStorage Indicates if this secondary node will replicate blobs in Object Storage
 * @property url The user-facing URL for this Geo node
 * @property verificationMaxCapacity The maximum concurrency of repository verification for this secondary node
 */
data class GeoNode(
        val containerRepositoriesMaxCapacity: Int? = null,
        val enabled: Boolean? = null,
        val filesMaxCapacity: Int? = null,
        val id: ID,
        val internalUrl: String? = null,
        val minimumReverificationInterval: Int? = null,
        val name: String? = null,
        val primary: Boolean? = null,
        val reposMaxCapacity: Int? = null,
        val selectiveSyncShards: MutableList<String>? = null,
        val selectiveSyncType: String? = null,
        val syncObjectsStorage: Boolean? = null,
        val url: String? = null,
        val verificationMaxCapacity: Int? = null

)