package data.gitlab

/**
 * Represents a requirement
 *
 * @property author Author of the requirement
 * @property createdAt Timestamp of when the requirement was created
 * @property id ID of the requirement
 * @property iid Internal ID of the requirement
 * @property project Project to which the requirement belongs
 * @property state State of the requirement
 * @property title Title of the requirement
 * @property updatedAt Timestamp of when the requirement was last updated
 * @property userPermissions Permissions for the current user on the resource
 */
data class Requirement(
        val author: User,
        val createdAt: Time,
        val id: ID,
        val iid: ID,
        val project: Project,
        val state: RequirementState,
        val title: String?,
        val updatedAt: Time,
        val userPermissions: RequirementPermissions
)