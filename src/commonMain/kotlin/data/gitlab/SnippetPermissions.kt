package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property adminSnippet Indicates the user can perform admin_snippet on this resource
 * @property awardEmoji Indicates the user can perform award_emoji on this resource
 * @property createNote Indicates the user can perform create_note on this resource
 * @property readSnippet Indicates the user can perform read_snippet on this resource
 * @property reportSnippet Indicates the user can perform report_snippet on this resource
 * @property updateSnippet Indicates the user can perform update_snippet on this resource
 */
data class SnippetPermissions(
        val adminSnippet: Boolean,
        val awardEmoji: Boolean,
        val createNote: Boolean,
        val readSnippet: Boolean,
        val reportSnippet: Boolean,
        val updateSnippet: Boolean
)