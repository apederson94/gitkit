package data.gitlab

/**
 * A Sentry error. A simplified version of SentryDetailedError
 *
 * @property count Count of occurrences
 * @property culprit Culprit of the error
 * @property externalUrl External URL of the error
 * @property firstSeen Timestamp when the error was first seen
 * @property frequency Last 24hr stats of the error
 * @property id ID (global ID) of the error
 * @property lastSeen Timestamp when the error was last seen
 * @property message Sentry metadata message of the error
 * @property sentryId ID (Sentry ID) of the error
 * @property sentryProjectId ID of the project (Sentry project)
 * @property sentryProjectName Name of the project affected by the error
 * @property sentryProjectSlug Slug of the project affected by the error
 * @property shortId Short ID (Sentry ID) of the error
 * @property status Status of the error
 * @property title Title of the error
 * @property type Type of the error
 * @property userCount Count of users affected by the error
 */
data class SentryError(
        val count: Int,
        val culprit: String,
        val externalUrl: String,
        val firstSeen: Time,
        val frequency: MutableList<SentryErrorFrequency>?,
        val id: ID,
        val lastSeen: Time,
        val message: String?,
        val sentryId: String,
        val sentryProjectId: ID,
        val sentryProjectSlug: String,
        val shortId: String,
        val status: SentryErrorStatus,
        val title: String,
        val type: String,
        val userCount: Int
)