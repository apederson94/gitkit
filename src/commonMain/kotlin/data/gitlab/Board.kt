package data.gitlab

/**
 * Represents a project or group board
 *
 * @property id ID (global ID) of the board
 * @property name Name of the board
 * @property weight Weight of the board
 */
data class Board(
        val id: ID,
        val name: String? = null,
        val weight: Int? = null
)