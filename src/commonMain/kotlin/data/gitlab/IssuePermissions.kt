package data.gitlab

/**
 * Check permissions for the current user on a issue
 *
 * @property adminIssue Indicates the user can perform admin_issue on this resource
 * @property createDesign Indicates the user can perform create_design on this resource
 * @property createNote Indicates the user can perform create_note on this resource
 * @property destroyDesign Indicates the user can perform destroy_design on this resource
 * @property readDesign Indicates the user can perform read_design on this resource
 * @property readIssue Indicates the user can perform read_issue on this resource
 * @property reopenIssue Indicates the user can perform reopen_issue on this resource
 * @property updateIssue Indicates the user can perform update_issue on this resource
 */
data class IssuePermissions(
        val adminIssue: Boolean,
        val createDesign: Boolean,
        val createNote: Boolean,
        val destroyDesign: Boolean,
        val readDesign: Boolean,
        val readIssue: Boolean,
        val reopenIssue: Boolean,
        val updateIssue: Boolean
)