package data.gitlab

/**
 * A design pinned to a specific version. The image field reflects the design as of the associated version.
 *
 * @property design The underlying design
 * @property diffRefs The diff refs for this design
 * @property event How this design was changed in the current version
 * @property filename The filename of the design
 * @property fullPath The full path to the design file
 * @property id The ID of this design
 * @property image The URL of the full-sized image
 * @property imageV432x230 The URL of the design resized to fit within the bounds of 432x230. This will be null if the image has not been generated
 * @property issue The issue the design belongs to
 * @property notesCount The total count of user-created notes for this design
 * @property project The project the design belongs to
 * @property version The version this design-at-versions is pinned to
 */
data class DesignAtVersion(
        val design: Design,
        val diffRefs: DiffRefs,
        val event: DesignVersionEvent,
        val filename: String,
        val fullPath: String,
        val id: ID,
        val image: String,
        val imageV432x230: String? = null,
        val issue: Issue,
        val notesCount: Int,
        val project: Project,
        val version: DesignVersion
)