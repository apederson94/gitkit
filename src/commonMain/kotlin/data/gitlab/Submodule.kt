package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property flatPath Flat path of the entry
 * @property id ID of the entry
 * @property name Name of the entry
 * @property path Path of the entry
 * @property sha Last commit sha for the entry
 * @property treeUrl Tree URL for the sub-module
 * @property type Type of tree entry
 * @property webUrl Web URL for the sub-module
 */
data class Submodule(
        val flatPath: String,
        val id: ID,
        val name: String,
        val path: String,
        val sha: String,
        val treeUrl: String?,
        val type: EntryType,
        val webUrl: String?
)