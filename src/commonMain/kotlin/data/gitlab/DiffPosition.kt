package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property diffRefs Information about the branch, HEAD, and base at the time of commenting
 * @property filePath Path of the file that was changed
 * @property height Total height of the image
 * @property newLine Line on HEAD SHA that was changed
 * @property newPath Path of the file on the HEAD SHA
 * @property oldLine Line on start SHA that was changed
 * @property oldPath Path of the file on the start SHA
 * @property positionType Type of file the position refers to
 * @property width Total width of the image
 * @property x X position of the note
 * @property y Y position of the note
 */
data class DiffPosition(
        val diffRefs: DiffRefs,
        val filePath: String,
        val height: Int? = null,
        val newLine: Int? = null,
        val newPath: String? = null,
        val oldLine: Int? = null,
        val oldPath: String? = null,
        val positionType: DiffPositionType,
        val width: Int? = null,
        val x: Int? = null,
        val y: Int? = null
)