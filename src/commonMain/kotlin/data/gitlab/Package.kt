package data.gitlab

/**
 * Represents a package
 *
 * @property createdAt The created date
 * @property id The ID of the package
 * @property name The name of the package
 * @property packageType The type of the package
 * @property updatedAt The update date
 * @property version The version of the package
 */
data class Package(
        val createdAt: Time,
        val id: ID,
        val name: String,
        val packageType: PackageTypeEnum,
        val updatedAt: Time,
        val version: String?
)