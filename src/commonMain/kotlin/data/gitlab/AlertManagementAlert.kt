package data.gitlab

/**
 * Describes an alert from the project’s Alert Management
 *
 * @property assignees Assignees of the alert
 * @property createdAt Timestamp the alert was created
 * @property description Description of the alert
 * @property details Alert details
 * @property endedAt Timestamp the alert ended
 * @property eventCount Number of events of this alert
 * @property hosts List of hosts the alert came from
 * @property iid Internal ID of the alert
 * @property issueIid Internal ID of the GitLab issue attached to the alert
 * @property monitoringTool Monitoring tool the alert came from
 * @property service Service the alert came from
 * @property severity Severity of the alert
 * @property startedAt Timestamp the alert was raised
 * @property status Status of the alert
 * @property title Title of the alert
 * @property updatedAt Timestamp the alert was last updated
 */
data class AlertManagementAlert(
        val assignees: MutableList<User>? = null,
        val createdAt: Time? = null, //TODO: Change to Date somehow
        val description: String? = null,
        val details: JSON? = null, //TODO: Get JSON integrated
        val endedAt: Time? = null, //TODO: Change to Date somehow
        val eventCount: Int? = null,
        val hosts: MutableList<String>? = null,
        val iid: ID, //TODO: Figure out what ID is
        val issueIid: ID? = null, //TODO: Figure out what ID is
        val monitoringTool: String? = null,
        val service: String? = null,
        val severity: AlertManagementSeverity? = null,
        val startedAt: Time? = null,
        val status: AlertManagmentStatus? = null,
        val title: String? = null,
        val updatedAt: Time? = null
)