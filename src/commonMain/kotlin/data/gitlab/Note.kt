package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property author User who wrote this note
 * @property body Content of the note
 * @property bodyHtml The GitLab Flavored Markdown rendering of [note]
 * @property confidential Indicates if this note is confidential
 * @property createdAt Timestamp of the note creation
 * @property discussion The discussion this note is a part of
 * @property id ID of the note
 * @property position The position of this note on a diff
 * @property project Project associated with the note
 * @property resolvable Indicates if the object can be resolved
 * @property resolved Indicates if the object is resolved
 * @property resolvedAt Timestamp of when the object was resolved
 * @property resolvedBy User who resolved the object
 * @property system Indicates whether this note was created by the system or by a user
 * @property updatedAt Timestamp of the note’s last activity
 * @property userPermissions Permissions for the current user on the resource
 */
data class Note(
        val author: User,
        val body: String,
        val bodyHtml: String?,
        val confidential: Boolean?,
        val createdAt: Time,
        val discussion: Discussion?,
        val id: ID,
        val position: DiffPosition?,
        val project: Project?,
        val resolvable: Boolean,
        val resolved: Boolean,
        val resolvedAt: Time?,
        val resolvedBy: User?,
        val system: Boolean,
        val updatedAt: Time,
        val userPermissions: NotePermissions
)