package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property buildArtifactsSize The CI artifacts size in bytes
 * @property lfsObjectsSize The LFS objects size in bytes
 * @property packagesSize The packages size in bytes
 * @property repositorySize The Git repository size in bytes
 * @property storageSize The total storage in bytes
 * @property wikiSize The wiki size in bytes
 */
data class RootStorageStatistics(
        val buildArtifactsSize: Float,
        val lfsObjectsSize: Float,
        val packagesSize: Float,
        val repositorySize: Float,
        val storageSize: Float,
        val wikiSize: Float
)