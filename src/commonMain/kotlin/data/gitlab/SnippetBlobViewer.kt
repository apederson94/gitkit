package data.gitlab

/**
 * Represents how the blob content should be displayed
 *
 * @property collapsed Shows whether the blob should be displayed collapsed
 * @property fileType Content file type
 * @property loadAsync Shows whether the blob content is loaded async
 * @property loadingPartialName Loading partial name
 * @property renderError Error rendering the blob content
 * @property tooLarge Shows whether the blob too large to be displayed
 * @property type Type of blob viewer
 */
data class SnippetBlobViewer(
        val collapsed: Boolean,
        val fileType: String,
        val loadAsync: Boolean,
        val loadingPartialName: String,
        val renderError: String?,
        val tooLarge: Boolean,
        val type: BlobViewersType
)