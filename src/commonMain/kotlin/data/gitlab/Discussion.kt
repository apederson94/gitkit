package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property createdAt Timestamp of the discussion’s creation
 * @property id ID of this discussion
 * @property replyId ID used to reply to this discussion
 * @property resolvable Indicates if the object can be resolved
 * @property resolved Indicates if the object is resolved
 * @property resolvedAt Timestamp of when the object was resolved
 * @property resolvedBy User who resolved the object
 */
data class Discussion(
        val createdAt: Time,
        val id: ID,
        val replyId: ID,
        val resolvable: Boolean,
        val resolved: Boolean,
        val resolvedAt: Time? = null,
        val resolvedBy: User? = null
)