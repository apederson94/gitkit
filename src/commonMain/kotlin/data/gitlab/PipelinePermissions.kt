package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property adminPipeline Indicates the user can perform admin_pipeline on this resource
 * @property destroyPipeline Indicates the user can perform destroy_pipeline on this resource
 * @property updatePipeline Indicates the user can perform destroy_pipeline on this resource
 */
data class PipelinePermissions(
        val adminPipeline: Boolean,
        val destroyPipeline: Boolean,
        val updatePipeline: Boolean
)