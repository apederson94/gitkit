package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property description Description of the namespace
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property fullName Full name of the namespace
 * @property fullPath Full path of the namespace
 * @property id ID of the namespace
 * @property lfsEnabled Indicates if Large File Storage (LFS) is enabled for namespace
 * @property name Name of the namespace
 * @property path Path of the namespace
 * @property requestAccessEnabled Indicates if users can request access to namespace
 * @property rootStorageStatistics Aggregated storage statistics of the namespace. Only available for root namespaces
 * @property visibility Visibility of the namespace
 */
data class Namespace(
        val description: String? = null,
        val descriptionHtml: String? = null,
        val fullName: String,
        val fullPath: ID,
        val id: ID,
        val lfsEnabled: Boolean? = null,
        val name: String,
        val path: String,
        val requestAccessEnabled: Boolean? = null,
        val rootStorageStatistics: RootStorageStatistics? = null,
        val visibility: String? = null
)