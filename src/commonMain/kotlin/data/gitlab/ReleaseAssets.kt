package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property assetsCount Number of assets of the release
 */
data class ReleaseAssets(
        val assetsCount: Int?
)