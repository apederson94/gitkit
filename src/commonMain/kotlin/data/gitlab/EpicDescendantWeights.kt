package data.gitlab

/**
 * Total weight of open and closed descendant issues
 *
 * @property closedIssues Total weight of completed (closed) issues in this epic, including epic descendants
 * @property openedIssues Total weight of opened issues in this epic, including epic descendants
 */
data class EpicDescendantWeights(
        val closedIssues: Int? = null,
        val openedIssues: Int? = null
)