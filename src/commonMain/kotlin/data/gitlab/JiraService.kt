package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property active Indicates if the service is active
 * @property projects List of Jira projects fetched through Jira REST API
 * @property type Class name of the service
 */
data class JiraService(
        val active: Boolean? = null,
        val projects: JiraProjectConnection? = null,
        val type: String?
)