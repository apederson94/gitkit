package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property color Background color of the label
 * @property description Description of the label (Markdown rendered as HTML for caching)
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property id Label ID
 * @property textColor Text color of the label
 * @property title Content of the label
 */
data class Label(
        val color: String,
        val description: String? = null,
        val descriptionHtml: String? = null,
        val id: ID,
        val textColor: String,
        val title: String
)