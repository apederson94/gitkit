package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property alertManagementAlert A single Alert Management alert of the project
 * @property alertManagementAlertStatusCountsType Counts of alerts by status for the project
 * @property archived Indicates the archived status of the project
 * @property autocloseReferencedIssues Indicates if issues referenced by merge requests and commits within the default branch are closed automatically
 * @property avatarUrl URL to avatar image file of the project
 * @property board A single board of the project
 * @property containerExpirationPolicy The container expiration policy of the project
 * @property containerRegistryEnabled Indicates if the project stores Docker container images in a container registry
 * @property createdAt Timestamp of the project creation
 * @property description Short description of the project
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property forksCount Number of times the project has been forked
 * @property fullPath Full path of the project
 * @property grafanaIntegration Grafana integration details for the project
 * @property group Group of the project
 * @property httpUrlToRepo URL to connect to the project via HTTPS
 * @property importStatus Status of import background job of the project
 * @property issue A single issue of the project
 * @property issuesEnabled Indicates if Issues are enabled for the current user
 * @property jiraImportStatus Status of Jira import background job of the project
 * @property jobsEnabled Indicates if CI/CD pipeline jobs are enabled for the current user
 * @property label A label available on this project
 * @property lastActivityAt Timestamp of the project last activity
 * @property lfsEnabled Indicates if the project has Large File Storage (LFS) enabled
 * @property mergeRequest A single merge request of the project
 * @property mergeRequestsEnabled Indicates if Merge Requests are enabled for the current user
 * @property mergeRequestsFfOnlyEnabled Indicates if no merge commits should be created and all merges should instead be fast-forwarded, which means that merging is only allowed if the branch could be fast-forwarded.
 * @property name Name of the project (without namespace)
 * @property nameWithNamespace Full name of the project with its namespace
 * @property namespace Namespace of the project
 * @property onlyAllowMergeIfAllDiscussionsAreResolved Indicates if merge requests of the project can only be merged when all the discussions are resolved
 * @property onlyAllowMergeIfPipelineSucceeds Indicates if merge requests of the project can only be merged with successful jobs
 * @property openIssuesCount Number of open issues for the project
 * @property path Path of the project
 * @property printingMergeRequestLinkEnabled Indicates if a link to create or view a merge request should display after a push to Git repositories of the project from the command line
 * @property publicJobs Indicates if there is public access to pipelines and job details of the project, including output logs and artifacts
 * @property release A single release of the project. Available only when feature flag graphql_release_data is enabled
 * @property removeSourceBranchAfterMerge Indicates if Delete source branch option should be enabled by default for all new merge requests of the project
 * @property repository Git repository of the project
 * @property requestAccessEnabled Indicates if users can request member access to the project
 * @property requirement Find a single requirement. Available only when feature flag requirements_management is enabled.
 * @property requirementStatesCount Number of requirements for the project by their state
 * @property sentryDetailedError Detailed version of a Sentry error on the project
 * @property sentryErrors Paginated collection of Sentry errors on the project
 * @property serviceDeskAddress E-mail address of the service desk.
 * @property serviceDeskEnabled Indicates if the project has service desk enabled.
 * @property sharedRunnersEnabled Indicates if Shared Runners are enabled for the project
 * @property snippetsEnabled Indicates if Snippets are enabled for the current user
 * @property sshUrlToRepo URL to connect to the project via SSH
 * @property starCount Number of times the project has been starred
 * @property statistics Statistics of the project
 * @property suggestionCommitMessage The commit message used to apply merge request suggestions
 * @property tagList List of project topics (not Git tags)
 * @property userPermissions Permissions for the current user on the resource
 * @property visibility Visibility of the project
 * @property vulnerabilitySeveritiesCount Counts for each severity of vulnerability of the project
 * @property webUrl Web URL of the project
 * @property wikiEnabled Indicates if Wikis are enabled for the current user
 */
data class Project(
        val alertManagementAlert: AlertManagementAlert?,
        val alertManagementAlertStatusCountsType: AlertManagementAlertStatusCountsType?,
        val archived: Boolean?,
        val autocloseReferencedIssues: Boolean?,
        val avatarUrl: String?,
        val board: Board?,
        val containerExpirationPolicy: ContainerExpirationPolicy?,
        val containerRegistryEnabled: Boolean?,
        val createdAt: Time?,
        val description: String?,
        val descriptionHtml: String?,
        val forksCount: Int,
        val fullPath: ID,
        val grafanaIntegration: GrafanaIntegration?,
        val group: Group?,
        val httpUrlToRepo: String?,
        val importStatus: String?,
        val issue: Issue?,
        val issuesEnabled: Boolean?,
        val jiraImportStatus: String?,
        val jobsEnabled: Boolean?,
        val label: Label?,
        val lastActivityAt: Time?,
        val lfsEnabled: Boolean?,
        val mergeRequest: MergeRequest?,
        val mergeRequestsEnabled: Boolean?,
        val mergeRequestsFfOnlyEnabled: Boolean?,
        val name: String,
        val nameWithNamespace: Namespace,
        val namespace: Namespace?,
        val onlyAllowMergeIfAllDiscussionsAreResolved: Boolean?,
        val onlyAllowMergeIfPipelineSucceeds: Boolean?,
        val openIssuesCount: Int?,
        val path: String,
        val printingMergeRequestLinkEnabled: Boolean?,
        val publicJobs: Boolean?,
        val release: Release?,
        val removeSourceBranchAfterMerge: Boolean?,
        val repository: Repository?,
        val requestAccessEnabled: Boolean?,
        val requirement: Requirement?,
        val requirementStatesCount: RequirementStatesCount?,
        val sentryDetailedError: SentryDetailedError?,
        val sentryErrors: SentryErrorCollection?,
        val serviceDeskAddress: String?,
        val serviceDeskEnabled: Boolean?,
        val sharedRunnersEnabled: Boolean?,
        val snippetsEnabled: Boolean?,
        val sshUrlToRepo: String?,
        val starCount: Int,
        val statistics: ProjectStatistics?,
        val suggestionCommitMessage: String?,
        val tagList: String?,
        val userPermissions: ProjectPermissions,
        val visibility: String?,
        val vulnerabilitySeveritiesCount: VulnerabilitySeveritiesCount?,
        val webUrl: String?,
        val wikiEnabled: Boolean?
)