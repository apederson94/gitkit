package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property createSnippet Indicates the user can perform create_snippet on this resource
 */
data class UserPermissions(
        val createSnippet: Boolean
)