package data.gitlab

/**
 * A tag expiration policy designed to keep only the images that matter most
 * 
 * @property cadence This container expiration policy schedule
 * @property createdAt Timestamp of when the container expiration policy was created
 * @property enabled Indicates whether this container expiration policy is enabled
 * @property keepN Number of tags to retain
 * @property nameRegex Tags with names matching this regex pattern will expire
 * @property nameRegexKeep Tags with names matching this regex pattern will be preserved
 * @property nextRunAt Next time that this container expiration policy will get executed
 * @property olderThan Tags older that this will expire
 * @property updatedAt Timestamp of when the container expiration policy was updated
 * 
 */
data class ContainerExpirationPolicy(
        val cadence: ContainerExpirationPolicyCadenceEnum,
        val createdAt: Time,
        val enabled: Boolean,
        val keepN: ContainerExpirationPolicyKeepEnum? = null,
        val nameRegex: String? = null,
        val nameRegexKeep: String? = null,
        val nextRunAt: Time? = null,
        val olderThan: ContainerExpirationPolicyOlderThanEnum? = null,
        val updatedAt: Time
)