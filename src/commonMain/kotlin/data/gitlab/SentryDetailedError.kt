package data.gitlab

/**
 * A Sentry error
 *
 * @property count Count of occurrences
 * @property culprit Culprit of the error
 * @property externalBaseUrl External Base URL of the Sentry Instance
 * @property externalUrl External URL of the error
 * @property firstReleaseLastCommit Commit the error was first seen
 * @property firstReleaseShortVersion Release version the error was first seen
 * @property firstSeen Timestamp when the error was first seen
 * @property frequency Last 24hr stats of the error
 * @property gitlabCommit GitLab commit SHA attributed to the Error based on the release version
 * @property gitlabCommitPath Path to the GitLab page for the GitLab commit attributed to the error
 * @property gitlabIssuePath URL of GitLab Issue
 * @property id ID (global ID) of the error
 * @property lastReleaseCommit Commit the error was last seen
 * @property lastReleaseShortVersion Release version the error was last seen
 * @property lastSeen Timestamp when the error was last seen
 * @property message Sentry metadata message of the error
 * @property sentryId ID (Sentry ID) of the error
 * @property entryProjectId ID of the project (Sentry project)
 * @property sentryProjectName Name of the project affected by the error
 * @property sentryProjectSlug Slug of the project affected by the error
 * @property shortId Short ID (Sentry ID) of the error
 * @property status Status of the error
 * @property tags Tags associated with the Sentry Error
 * @property title Title of the error
 * @property type Type of the error
 * @property userCount Count of users affected by the error
 */
data class SentryDetailedError(
        val count: Int,
        val culprit: String,
        val externalBaseUrl: String,
        val externalUrl: String,
        val firstReleaseLastCommit: String?,
        val firstReleaseShortVersion: String?,
        val firstSeen: Time,
        val frequency: MutableList<SentryErrorFrequency>?,
        val gitlabCommit: String?,
        val gitlabCommitPath: String?,
        val gitlabIssuePath: String?,
        val id: ID,
        val lastReleaseCommit: String?,
        val lastReleaseShortVersion: String?,
        val lastSeen: Time,
        val message: String?,
        val sentryId: String,
        val entryProjectId: ID,
        val sentryProjectName: String,
        val sentryProjectSlug: String,
        val shortId: String,
        val status: SentryErrorStatus,
        val tags: SentryErrorTags,
        val title: String,
        val type: String,
        val userCount: Int
)