package data.gitlab

/**
 * Represents total number of alerts for the represented categories
 *
 * @property acknowledged Number of alerts with status ACKNOWLEDGED for the project
 * @property all Total number of alerts for the project
 * @property ignored Number of alerts with status IGNORED for the project
 * @property open Number of alerts with status TRIGGERED or ACKNOWLEDGED for the project
 * @property resolved Number of alerts with status RESOLVED for the project
 * @property triggered Number of alerts with status TRIGGERED for the project
 */
data class AlertManagementAlertStatusCountsType(
        val acknowledged: Int? = null,
        val all: Int? = null,
        val ignored: Int? = null,
        val open: Int? = null,
        val resolved: Int? = null,
        val triggered: Int? = null
)