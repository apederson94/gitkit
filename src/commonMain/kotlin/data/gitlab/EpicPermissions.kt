package data.gitlab

/**
 * Check permissions for the current user on an epic
 *
 * @property adminEpic Indicates the user can perform admin_epic on this resource
 * @property awardEmoji Indicates the user can perform award_emoji on this resource
 * @property createEpic Indicates the user can perform create_epic on this resource
 * @property createNote Indicates the user can perform create_note on this resource
 * @property destroyEpic Indicates the user can perform destroy_epic on this resource
 * @property readEpic Indicates the user can perform read_epic on this resource
 * @property readEpicIid Indicates the user can perform read_epic_iid on this resource
 * @property updateEpic Indicates the user can perform update_epic on this resource
 */
data class EpicPermissions(
        val adminEpic: Boolean,
        val awardEmoji: Boolean,
        val createEpic: Boolean,
        val createNote: Boolean,
        val destroyEpic: Boolean,
        val readEpic: Boolean,
        val readEpicIid: Boolean,
        val updateEpic: Boolean
)