package data.gitlab

/**
 * Represents a list for an issue board
 *
 * @property assignee Assignee in the list
 * @property collapsed Indicates if list is collapsed for this user
 * @property id ID (global ID) of the list
 * @property label Label of the list
 * @property limitMetric The current limit metric for the list
 * @property listType Type of the list
 * @property maxIssueCount Maximum number of issues in the list
 * @property maxIssueWeight Maximum weight of issues in the list
 * @property milestone Milestone of the list
 * @property position Position of list within the board
 * @property title Title of the list
 */
data class BoardList(
        val assignee: User? = null,
        val collapsed: Boolean? = null,
        val id: ID,
        val label: Label? = null,
        val limitMetric: ListLimitMetric? = null,
        val listType: String,
        val maxIssueCount: Int? = null,
        val maxIssueWeight: Int? = null,
        val milestone: Milestone? = null,
        val position: Int? = null,
        val title: String
)