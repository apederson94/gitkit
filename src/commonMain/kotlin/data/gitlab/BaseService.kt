package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property active Indicates if the service is active
 * @property type Class name of the service
 */
data class BaseService(
        val active: Boolean? = null,
        val type: String? = null
)