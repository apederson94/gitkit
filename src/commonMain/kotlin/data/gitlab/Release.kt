package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property assets Assets of the release
 * @property author User that created the release
 * @property commit The commit associated with the release
 * @property createdAt Timestamp of when the release was created
 * @property description Description (also known as “release notes”) of the release
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property name Name of the release
 * @property releasedAt Timestamp of when the release was released
 * @property tagName Name of the tag associated with the release
 * @property tagPath Relative web path to the tag associated with the release
 */
data class Release(
        val assets: ReleaseAssets?,
        val author: User?,
        val commit: Commit?,
        val createdAt: Time?,
        val description: String,
        val descriptionHtml: String?,
        val name: String?,
        val releasedAt: Time?,
        val tagName: String,
        val tagPath: String?
)