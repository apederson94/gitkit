package data.gitlab

/**
 * Represents a milestone
 *
 * @property createdAt Timestamp of milestone creation
 * @property description Description of the milestone
 * @property dueDate Timestamp of the milestone due date
 * @property groupMilestone Indicates if milestone is at group level
 * @property id ID of the milestone
 * @property projectMilestone Indicates if milestone is at project level
 * @property startDate Timestamp of the milestone start date
 * @property state State of the milestone
 * @property subgroupMilestone Indicates if milestone is at subgroup level
 * @property title Title of the milestone
 * @property updatedAt Timestamp of last milestone update
 * @property webPath Web path of the milestone
 */
data class Milestone(
        val createdAt: Time,
        val description: String?,
        val dueDate: Time?,
        val groupMilestone: Boolean,
        val id: ID,
        val projectMilestone: Boolean,
        val startDate: Time?,
        val state: MilestoneStateEnum,
        val subgroupMilestone: Boolean,
        val title: String,
        val updatedAt: Time,
        val webPath: String
)