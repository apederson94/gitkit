package data.gitlab

/**
 * An object containing a stack trace entry for a Sentry error.
 *
 * @property col Function in which the Sentry error occurred
 * @property fileName File in which the Sentry error occurred
 * @property function Function in which the Sentry error occurred
 * @property line Function in which the Sentry error occurred
 * @property traceContext Context of the Sentry error
 */
data class SentryErrorStackTraceEntry(
        val col: String?,
        val fileName: String?,
        val function: String?,
        val line: String?,
        val traceContext: MutableList<SentryErrorStackTraceContext>
)