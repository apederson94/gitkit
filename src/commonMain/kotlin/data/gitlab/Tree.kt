package data.gitlab

/**
 * Description not provided by the API documentation
 *
 * @property lastCommit
 */
data class Tree(
        val lastCommit: Commit?
)