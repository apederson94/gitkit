package data.gitlab

/**
 * An object containing a collection of Sentry errors, and a detailed error
 *
 * @property detailedError Detailed version of a Sentry error on the project
 * @property errorStackTrace Stack Trace of Sentry Error
 * @property errors Collection of Sentry Errors
 * @property externalUrl External URL for Sentry
 */
data class SentryErrorCollection(
        val detailedError: SentryDetailedError?,
        val errorStackTrace: SentryErrorStackTrace?,
        val errors: SentryErrorCollection?,
        val externalUrl: String?
)