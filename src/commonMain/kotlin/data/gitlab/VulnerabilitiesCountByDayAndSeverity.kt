package data.gitlab

/**
 * Represents the number of vulnerabilities for a particular severity on a particular day
 *
 * @property count
 * @property day
 * @property severity
 */
data class VulnerabilitiesCountByDayAndSeverity(
        val count: Int?,
        val day: ISO8601Date?,
        val severity: VulnerabilitySeverity?
)