package data.gitlab

/**
 * Health status of child issues
 *
 * @property issuesAtRisk Number of issues at risk
 * @property issuesNeedingAttention Number of issues that need attention
 * @property issuesOnTrack Number of issues on track
 */
data class EpicHealthStatus(
        val issuesAtRisk: Int? = null,
        val issuesNeedingAttention: Int? = null,
        val issuesOnTrack: Int? = null
)