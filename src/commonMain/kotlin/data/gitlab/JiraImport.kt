package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property createdAt Timestamp of when the Jira import was created
 * @property failedToImportCount Count of issues that failed to import
 * @property importedIssuesCount Count of issues that were successfully imported
 * @property jiraProjectKey Project key for the imported Jira project
 * @property scheduledAt Timestamp of when the Jira import was scheduled
 * @property scheduledBy User that started the Jira import
 * @property totalIssueCount Total count of issues that were attempted to import
 */
data class JiraImport(
        val createdAt: Time? = null,
        val failedToImportCount: Int,
        val importedIssuesCount: Int,
        val jiraProjectKey: String,
        val scheduledAt: Time? = null,
        val scheduledBy: User? = null,
        val totalIssueCount: Int
)