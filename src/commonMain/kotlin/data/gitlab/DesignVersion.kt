package data.gitlab

/**
 * A specific version in which designs were added, modified or deleted
 *
 * @property designAtVersion A particular design as of this version, provided it is visible at this version
 * @property id ID of the design version
 * @property sha SHA of the design version
 */
data class DesignVersion(
        val designAtVersion: DesignAtVersion,
        val id: ID,
        val sha: ID
)