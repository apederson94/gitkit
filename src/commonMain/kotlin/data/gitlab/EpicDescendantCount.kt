package data.gitlab

/**
 * Counts of descendent epics.
 *
 * @property closedEpics Number of closed sub-epics
 * @property closedIssues Number of closed epic issues
 * @property openedEpics Number of opened sub-epics
 * @property openedIssues Number of opened epic issues
 */
data class EpicDescendantCount(
        val closedEpics: Int? = null,
        val closedIssues: Int? = null,
        val openedEpics: Int? = null,
        val openedIssues: Int? = null
)