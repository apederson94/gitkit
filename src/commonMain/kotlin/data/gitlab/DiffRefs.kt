package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property baseSha Merge base of the branch the comment was made on
 * @property headSha SHA of the HEAD at the time the comment was made
 * @property startSha SHA of the branch being compared against
 */
data class DiffRefs(
        val baseSha: String? = null,
        val headSha: String,
        val startSha: String
)