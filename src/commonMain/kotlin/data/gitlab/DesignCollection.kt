package data.gitlab

/**
 * A collection of designs.
 *
 * @property design Find a specific design
 * @property designAtVersion Find a design as of a version
 * @property issue Issue associated with the design collection
 * @property project Project associated with the design collection
 * @property version A specific version
 */
data class DesignCollection(
        val design: Design? = null,
        val designAtVersion: DesignAtVersion? = null,
        val issue: Issue,
        val project: Project,
        val version: DesignVersion? = null
)