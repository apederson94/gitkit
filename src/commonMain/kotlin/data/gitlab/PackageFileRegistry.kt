package data.gitlab

/**
 * Represents the sync and verification state of a package file
 *
 * @property createdAt Timestamp when the PackageFileRegistry was created
 * @property id ID of the PackageFileRegistry
 * @property lastSyncFailure Error message during sync of the PackageFileRegistry
 * @property lastSyncedAt Timestamp of the most recent successful sync of the PackageFileRegistry
 * @property packageFileId ID of the PackageFile
 * @property retryAt Timestamp after which the PackageFileRegistry should be resynced
 * @property retryCount Number of consecutive failed sync attempts of the PackageFileRegistry
 * @property state Sync state of the PackageFileRegistry
 */
data class PackageFileRegistry(
        val createdAt: Time?,
        val id: ID,
        val lastSyncFailure: String?,
        val lastSyncedAt: Time?,
        val packageFileId: ID,
        val retryAt: Time?,
        val retryCount: Int?,
        val state: RegistryState?
)