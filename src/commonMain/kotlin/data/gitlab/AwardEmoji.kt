package data.gitlab

/**
 * An emoji awarded by a user.
 *
 * @property description The emoji description
 * @property emoji The emoji as an icon
 * @property name The emoji name
 * @property unicode The emoji in unicode
 * @property unicodeVersion The unicode version for this emoji
 * @property user The user who awarded the emoji
 */
data class AwardEmoji(
        val description: String,
        val emoji: String,
        val name: String,
        val unicode: String,
        val unicodeVersion: String,
        val user: User
)