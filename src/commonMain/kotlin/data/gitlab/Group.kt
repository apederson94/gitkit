package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property autoDevopsEnabled Indicates whether Auto DevOps is enabled for all projects within this group
 * @property avatarUrl Avatar URL of the group
 * @property board A single board of the group
 * @property description Description of the namespace
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property emailsDisabled Indicates if a group has email notifications disabled
 * @property epic Find a single epic
 * @property epicsEnabled Indicates if Epics are enabled for namespace
 * @property fullName Full name of the namespace
 * @property fullPath Full path of the namespace
 * @property groupTimelogsEnabled Indicates if Group timelogs are enabled for namespace
 * @property id ID of the namespace
 * @property label A label available on this group
 * @property lfsEnabled Indicates if Large File Storage (LFS) is enabled for namespace
 * @property mentionsDisabled Indicates if a group is disabled from getting mentioned
 * @property name Name of the namespace
 * @property parent Parent group
 * @property path Path of the namespace
 * @property projectCreationLevel The permission level required to create projects in the group
 * @property requestAccessEnabled Indicates if users can request access to namespace
 * @property requireTwoFactorAuthentication Indicates if all users in this group are required to set up two-factor authentication
 * @property rootStorageStatistics Aggregated storage statistics of the namespace. Only available for root namespaces
 * @property shareWithGroupLock Indicates if sharing a project with another group within this group is prevented
 * @property subgroupCreationLevel The permission level required to create subgroups within the group
 * @property twoFactorGracePeriod Time before two-factor authentication is enforced
 * @property userPermissions Permissions for the current user on the resource
 * @property visibility Visibility of the namespace
 * @property webUrl Web URL of the group
 */
data class Group(
        val autoDevopsEnabled: Boolean? = null,
        val avatarUrl: String? = null,
        val board: Board? = null,
        val description: String? = null,
        val descriptionHtml: String? = null,
        val emailsDisabled: Boolean? = null,
        val epic: Epic? = null,
        val epicsEnabled: Boolean? = null,
        val fullName: String,
        val fullPath: ID,
        val groupTimelogsEnabled: Boolean? = null,
        val id: ID,
        val label: Label? = null,
        val lfsEnabled: Boolean? = null,
        val mentionsDisabled: Boolean? = null,
        val name: String,
        val parent: Group? = null,
        val path: String,
        val projectCreationLevel: String? = null,
        val requestAccessEnabled: Boolean? = null,
        val requireTwoFactorAuthentication: Boolean? = null,
        val rootStorageStatistics: RootStorageStatistics? = null,
        val shareWithGroupLock: Boolean? = null,
        val subgroupCreationLevel: String? = null,
        val twoFactorGracePeriod: Int? = null,
        val userPermissions: GroupPermissions,
        val visibility: String? = null,
        val webUrl: String
)