package data.gitlab

/**
 * Represents a snippet entry
 *
 * @property author The owner of the snippet
 * @property blob Snippet blob
 * @property blobs Snippet blobs
 * @property createdAt Timestamp this snippet was created
 * @property description Description of the snippet
 * @property descriptionHtml The GitLab Flavored Markdown rendering of [description]
 * @property fileName File Name of the snippet
 * @property httpUrlToRepo HTTP URL to the snippet repository
 * @property id ID of the snippet
 * @property project The project the snippet is associated with
 * @property rawUrl Raw URL of the snippet
 * @property sshUrlToRepo SSH URL to the snippet repository
 * @property title Title of the snippet
 * @property updatedAt Timestamp this snippet was updated
 * @property userPermissions Permissions for the current user on the resource
 * @property visibilityLevel Visibility Level of the snippet
 * @property webUrl Web URL of the snippet
 */
data class Snippet(
        val author: User?,
        val blob: SnippetBlob,
        val blobs: MutableList<SnippetBlob>?,
        val createdAt: Time,
        val description: String?,
        val descriptionHtml: String?,
        val fileName: String?,
        val httpUrlToRepo: String?,
        val id: ID,
        val project: Project?,
        val rawUrl: String,
        val sshUrlToRepo: String?,
        val title: String,
        val updatedAt: Time,
        val userPermissions: SnippetPermissions,
        val visibilityLevel: VisibilityLevelsEnum,
        val webUrl: String
)