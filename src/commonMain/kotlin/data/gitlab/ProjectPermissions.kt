package data.gitlab

/**
 * Description not provided by API documentation
 *
 * @property adminOperations Indicates the user can perform admin_operations on this resource
 * @property adminProject Indicates the user can perform admin_project on this resource
 * @property adminRemoteMirror Indicates the user can perform admin_remote_mirror on this resource
 * @property adminWiki Indicates the user can perform admin_wiki on this resource
 * @property archiveProject Indicates the user can perform archive_project on this resource
 * @property changeNamespace Indicates the user can perform change_namespace on this resource
 * @property changeVisibilityLevel Indicates the user can perform change_visibility_level on this resource
 * @property createDeployment Indicates the user can perform create_deployment on this resource
 * @property createDesign Indicates the user can perform create_design on this resource
 * @property createIssue Indicates the user can perform create_issue on this resource
 * @property createLabel Indicates the user can perform create_label on this resource
 * @property createMergeRequestFrom Indicates the user can perform create_merge_request_from on this resource
 * @property createMergeRequestsIn Indicates the user can perform create_merge_request_in on this resource
 * @property createPages Indicates the user can perform create_pages on this resource
 * @property createPipeline Indicates the user can perform create_pipeline on this resource
 * @property createPipelineSchedule Indicates the user can perform create_pipeline_schedule on this resource
 * @property createSnippet Indicates the user can perform create_snippet on this resource
 * @property createWiki Indicates the user can perform create_wiki on this resource
 * @property destroyDesign Indicates the user can perform destroy_design on this resource
 * @property destroyPages Indicates the user can perform destroy_pages on this resource
 * @property destroyWiki Indicates the user can perform destroy_wiki on this resource
 * @property downloadCode Indicates the user can perform download_code on this resource
 * @property downloadWikiCode Indicates the user can perform download_wiki_code on this resource
 * @property forkProject Indicates the user can perform fork_project on this resource
 * @property pushCode Indicates the user can perform push_code on this resource
 * @property pushToDeleteProtectedBranch Indicates the user can perform push_to_delete_protected_branch on this resource
 * @property readCommitStatus Indicates the user can perform read_commit_status on this resource
 * @property readCycleAnalytics Indicates the user can perform read_cycle_analytics on this resource
 * @property readDesign Indicates the user can perform read_design on this resource
 * @property readMergeRequest Indicates the user can perform read_merge_request on this resource
 * @property readPagesContent Indicates the user can perform read_pages_content on this resource
 * @property readProject Indicates the user can perform read_project on this resource
 * @property readProjectMember Indicates the user can perform read_project_member on this resource
 * @property readWiki Indicates the user can perform read_wiki on this resource
 * @property removeForkProject Indicates the user can perform remove_fork_project on this resource
 * @property removePages Indicates the user can perform remove_pages on this resource
 * @property removeProject Indicates the user can perform remove_project on this resource
 * @property renameProject Indicates the user can perform rename_project on this resource
 * @property requestAccess Indicates the user can perform request_access on this resource
 * @property updatePages Indicates the user can perform update_pages on this resource
 * @property updateWiki Indicates the user can perform update_wiki on this resource
 * @property updateFile Indicates the user can perform upload_file on this resource
 */
data class ProjectPermissions(
        val adminOperations: Boolean,
        val adminProject: Boolean,
        val adminRemoteMirror: Boolean,
        val adminWiki: Boolean,
        val archiveProject: Boolean,
        val changeNamespace: Boolean,
        val changeVisibilityLevel: Boolean,
        val createDeployment: Boolean,
        val createDesign: Boolean,
        val createIssue: Boolean,
        val createLabel: Boolean,
        val createMergeRequestFrom: Boolean,
        val createMergeRequestsIn: Boolean,
        val createPages: Boolean,
        val createPipeline: Boolean,
        val createPipelineSchedule: Boolean,
        val createSnippet: Boolean,
        val createWiki: Boolean,
        val destroyDesign: Boolean,
        val destroyPages: Boolean,
        val destroyWiki: Boolean,
        val downloadCode: Boolean,
        val downloadWikiCode: Boolean,
        val forkProject: Boolean,
        val pushCode: Boolean,
        val pushToDeleteProtectedBranch: Boolean,
        val readCommitStatus: Boolean,
        val readCycleAnalytics: Boolean,
        val readDesign: Boolean,
        val readMergeRequest: Boolean,
        val readPagesContent: Boolean,
        val readProject: Boolean,
        val readProjectMember: Boolean,
        val readWiki: Boolean,
        val removeForkProject: Boolean,
        val removePages: Boolean,
        val removeProject: Boolean,
        val renameProject: Boolean,
        val requestAccess: Boolean,
        val updatePages: Boolean,
        val updateWiki: Boolean,
        val updateFile: Boolean

)