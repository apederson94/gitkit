package data.gitlab

/**
 * Information about pagination in a connection.
 *
 * @property endCursor When paginating forwards, the cursor to continue.
 * @property hasNextPage When paginating forwards, are there more items?
 * @property hasPreviousPage When paginating backwards, are there more items?
 * @property startCursor When paginating backwards, the cursor to continue.
 */
data class PageInfo(
        val endCursor: String?,
        val hasNextPage: Boolean,
        val hasPreviousPage: Boolean,
        val startCursor: String?
)